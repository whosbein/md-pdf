#!/usr/bin/env python

# this is used to parse through the gross and huge higlight.pack.js file and
# grab all the individual languages that are being looked for

import re

with open('highlight.pack.js') as src_file:
    jsfile = src_file.read()

pattern = re.compile(r'hljs\.registerLanguage\("(.*?)"')
result = re.findall(pattern, jsfile)
for item in result:
    print(item)
