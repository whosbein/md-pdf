FROM haskell:8

RUN apt-get update -y \
    && apt-get install -y -o Acquire::Retries=10 --no-install-recommends \
    texlive-latex-base \
    texlive-xetex \
    latex-xcolor \
    texlive-math-extra \
    texlive-latex-extra \
    texlive-fonts-extra \
    lua5.2 \
    texlive-bibtex-extra \
    fontconfig \
    unzip \
    lmodern

# will ease up the update process
# updating this env var will trigger the automatic build of the Docker image
ENV PANDOC_VERSION "2.7.2"

# install pandoc
RUN cabal update && cabal install pandoc-${PANDOC_VERSION}

WORKDIR /source

ENTRYPOINT ["/root/.cabal/bin/pandoc"]

CMD ["--help"]
