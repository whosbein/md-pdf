#!/bin/bash

UTILS_DIR=${HOME}/code/bash/md-pdf/utils/
OUTPUT_DIR=${HOME}/Downloads/

IN_MD=$1

# make /path/to/file.ext into just "file"
# from https://stackoverflow.com/a/2664746
OUT_FILE=${2##*/}
OUT_FILE=${OUT_FILE%.*}

# this is going to be weird because of how Docker treats the host networking
# going to make it an input param for better future use
# or something...
if [ -z "$3" ]
then
    HOST_NAME=docker.for.mac.host.internal
else
    HOST_NAME=$3
fi

# TODO: if only two input params, make $2 = $3
# TODO: strip .html and .pdf, if present, on $2 and $3

# generate html (including headers in util dir)
docker run --rm \
    -v $(pwd):/source \
    -v $UTILS_DIR:/utils \
    -v $OUTPUT_DIR:/output \
    fandancy-pandoc:2.7.2 \
    --quiet \
    --no-highlight \
    -f gfm \
    -s --self-contained -H /utils/header/highlight.md -c /utils/css/github-markdown.css \
    /source/$IN_MD \
    -o /output/$OUT_FILE.html
    #--lua-filter /utils/filters/task-list.lua \

# start webserver
pushd $OUTPUT_DIR
python3 -m http.server 9100 &
WEBSERVER_PID=$!
sleep 2
popd

#user chrome headless from Docker container to print pdf of file
docker run --rm \
    --cap-add=SYS_ADMIN \
    -v $(pwd):/pdf \
    chrome-headless-node:0.1 \
    --url http://$HOST_NAME:9100/$OUT_FILE.html \
    --pdf $OUT_FILE.pdf \
    --chrome-binary chromium-browser \
    --chrome-option=--no-sandbox
    #--user nobody \

kill $WEBSERVER_PID
