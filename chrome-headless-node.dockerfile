# add maintainer
FROM ubuntu:latest

RUN \
    apt-get update && \
    apt-get install -y curl chromium-browser

RUN curl -sL https://deb.nodesource.com/setup_11.x | bash -

RUN \
    apt-get update && \
    apt-get install -y nodejs

RUN npm i -g chrome-headless-render-pdf

WORKDIR /pdf

ENTRYPOINT ["chrome-headless-render-pdf"]
